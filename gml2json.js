function findNodes(array){
	var curObj = {};
	var nodes = [], links = [];
	var inNode = false, inEdge = false, prev, current, next, str;

	for (var i = 0; i < array.length; i++){
		prev = array[i-1] || ''; current = array[i]; next = array[i+1];

		if (inNode || inEdge) { //build obj
			str = current.slice(4); //relies on correct indentation
			var key = str.substr(0,str.indexOf(' ')), val = str.substr(str.indexOf(' ')+1);
			curObj[key] = val;
		}

		// find out where we are
		if (prev.indexOf('node') >= 0 && current.indexOf('[') >= 0){
			inNode = true;
		}
		if (inNode && next.indexOf(']') >= 0) {
			inNode = false;
			nodes.push(curObj);
			curObj = {};
		}
		if (prev.indexOf('edge') >= 0 && current.indexOf('[') >= 0){
			inEdge = true;
		}
		if (inEdge && next.indexOf(']') >= 0) {
			inEdge = false;
			links.push(curObj);
			curObj = {};
		}
	} //for loop

	return {'nodes':nodes, 'links':links};
}

var fs = require('fs');
var allArgs = process.argv, args = allArgs.slice(2);
var read = args[0], write = args[1];
fs.readFile(read, function(err, data){
	if (err) {console.warn(err); throw err;}
	var array = data.toString().split('\n');
	var nodes = findNodes(array);

	fs.writeFile(write, JSON.stringify(nodes, null, 2), function(error){
		if (error) throw error;
		console.log('check it out');
	});
});

// Doesn't currently get details of graph. Converts numbers to strings
